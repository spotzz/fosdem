# Fosdem

This project provides images to promote the [CentOS
Project](https://www.centos.org/) at [FOSDEM](https://fosdem.org/) events. 

## Website

![CentOS Connect](Final/centos-connect-website-c10.png)

![CentOS Connect](Final/centos-connect-website-banner-bg-c10.png)

## Social media

Reusable on vinyl banner.

### Without artistic motif

![CentOS Connect](Final/centos-connect.png)

### With artistic motif

![CentOS Connect](Final/centos-connect-c10.png)
![CentOS Connect](Final/centos-connect-c10-alternative-1.png)
![CentOS Connect](Final/centos-connect-c10-alternative-2.png)


## License

The CentOS promotional artwork is released under the terms of [Creative Commons
Attribution-ShareAlike 4.0 International Public
License](https://creativecommons.org/licenses/by-sa/4.0/legalcode), and usage
limited by the [CentOS Trademark
Guidelines](https://www.centos.org/legal/trademarks/).
